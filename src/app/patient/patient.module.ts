import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { PipesModule } from '../pipes/pipes.module';

import { PatientRoutingModule } from './patient-routing.module';
import { PatientFormComponent } from './patient-form/patient-form.component';
import { RegisterPatientComponent } from './register-patient/register-patient.component';
import { PatientListComponent } from './patient-list/patient-list.component';


@NgModule({
  declarations: [PatientFormComponent, RegisterPatientComponent, PatientListComponent],
  imports: [
    CommonModule,
    PatientRoutingModule,
    SharedModule,
    PipesModule
  ],
  exports: []
})
export class PatientModule { }
