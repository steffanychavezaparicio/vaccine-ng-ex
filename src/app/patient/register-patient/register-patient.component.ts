import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


import { PatientService } from '../../shared/service/patientService/patient.service';
import { Patient } from 'src/app/shared/model/patient';

@Component({
  selector: 'app-register-patient',
  templateUrl: './register-patient.component.html',
  styleUrls: ['./register-patient.component.scss']
})
export class RegisterPatientComponent implements OnInit {

  constructor(private patientService: PatientService, private router: Router) { }

  ngOnInit(): void {
  }

  registerPatient(patient: Patient) {
    this.patientService.registerPatient(patient);
    this.router.navigateByUrl('/patients/patient-list');
  }

}
