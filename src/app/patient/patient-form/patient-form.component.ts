import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Patient } from 'src/app/shared/model/patient';
import { Vaccine } from 'src/app/shared/model/vaccine';
import { VaccineService } from 'src/app/shared/service/vaccineService/vaccine.service';

@Component({
  selector: 'app-patient-form',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.scss']
})
export class PatientFormComponent implements OnInit {
  form: FormGroup;
  @Input() patient: Patient;
  vaccineList: Vaccine[];
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onSubmit: EventEmitter<any>;

  constructor(private vaccineService: VaccineService) {
    this.onSubmit = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.patient = new Patient();
    this.getVaccineList();
  }

  save() {
    this.onSubmit.emit(this.patient);
  }

  getVaccineList() {
    this.vaccineList = this.vaccineService.getVaccineList();
  }

}
