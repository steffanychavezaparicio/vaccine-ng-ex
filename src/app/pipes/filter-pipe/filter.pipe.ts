import { Pipe, PipeTransform } from '@angular/core';
import { Patient } from '../../shared/model/patient';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: Patient[], patientId: string): any {
    if (patientId === '') {
      return value;
    }
    const patientList: Patient[] = [];
    value.forEach(patient => {
      const id: string = patient.id;
      if (id.startsWith(patientId)) {
        patientList.push(patient);
      }
    });
    return patientList;
  }

}
