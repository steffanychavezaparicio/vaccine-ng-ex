import { Injectable, getPlatform } from '@angular/core';
import { Patient } from '../../model/patient';
import { VaccineService } from '../vaccineService/vaccine.service';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  private PATH = 'patientList';

  constructor(private vaccineService: VaccineService) { }

  getPatientList() {
    let patientList: Array<Patient>;
    if (localStorage.getItem(this.PATH)) {
      patientList = JSON.parse(localStorage.getItem(this.PATH));
    } else { patientList = []; }
    return patientList;
  }

  getPatientById(id: string) {
    const patientList = this.getPatientList();
    const patient = patientList.find(item => item.id === id);
    console.log(patient);
    return patient;
  }

  registerPatient(patient: Patient) {
    let list = this.getPatientList();
    if (!this.patientExists(patient.id)) {
      list = [patient, ...list];
      this.savePatientList(list);
    }
  }

  updatePatient(patient: Patient) {
    if (this.patientExists(patient.id)) {
      const list = this.getPatientList();
      const index = list.findIndex(v => v.id === patient.id);
      list[index] = patient;
      this.savePatientList(list);
    } else {
      console.log('Patient not registered!');
    }
  }

  deletePatient(patient: Patient) {
    if (this.patientExists(patient.id)) {
      let list = this.getPatientList();
      list = list.filter(item => item.id !== patient.id);
      this.savePatientList(list);
    } else {
      console.log('Patient not registered!');
    }
  }

  savePatientList(patientList: Patient[]) {
    localStorage.setItem(this.PATH, JSON.stringify(patientList));
  }

  patientExists(id: string) {
    const list = this.getPatientList();
    const isRegistered = list.some(item => item.id === id);
    return isRegistered;
  }

  calculateSecondDoseDate(firstDoseDate: Date, timeBetweenDoses: number) {
    const secondDoseDate = new Date();
    const firstDose = new Date(firstDoseDate);
    secondDoseDate.setDate(firstDose.getDate() + timeBetweenDoses);
    return secondDoseDate;
  }

  setFirstDoseDate(patient: Patient, firstDose: Date) {
    patient.firstDoseDate = firstDose;
    patient.firstDose = true;
    this.vaccineService.subtractDoseOf(patient.vaccine);
    this.updatePatient(patient);
  }

  setSecondDoseDate(patient: Patient, secondDose: Date) {
    patient.secondDoseDate = secondDose;
    patient.secondDose = true;
    this.vaccineService.subtractDoseOf(patient.vaccine);
    this.updatePatient(patient);
  }

  getPatientVaccine(patientId: string) {
    const patient = this.getPatientById(patientId);
    const vaccine = this.vaccineService.getVaccineById(patient.vaccine);
    return vaccine;
  }
}
