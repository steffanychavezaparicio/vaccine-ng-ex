import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterVaccineComponent } from './register-vaccine/register-vaccine.component';
import { VaccineListComponent } from './vaccine-list/vaccine-list.component';

const routes: Routes = [
  { path: 'register-vaccine', component: RegisterVaccineComponent },
  { path: 'vaccine-list', component: VaccineListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VaccineRoutingModule { }
