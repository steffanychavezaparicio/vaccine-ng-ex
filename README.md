# Control de Vacunación SEDES

Este sistema fue creado con el objetivo de mantener un correcto seguimiento de los casos de personas vacunadas para Covid-19, para evitar problemas como una doble vacuna suministrada a un paciente o confusion sobre las fechas y proveedores.

## Comenzando 🚀

Luego de obtener una copia del proyecto en tu máquina local, ingresa el comando `ng serve` y navega a `http://localhost:4200/`. La aplicación se actualizara automaticamente si se realizan cambios en el código.

Para generar un build, ingresa el comando `ng build`, para el modo producción utiliza el flag `--prod`.

### Pre-requisitos 📋

* NodeJS - Permite instalar las dependencias necesarias para la aplicación.
* AngularCLI - Para la creacion y manejo de proyectos en Angular.

### Instalación 🔧

* Clonar el Proyecto
    - git clone https://gitlab.com/steffanychavezaparicio/vaccine-ng-ex.git
    - cd vaccine-ng-ex
* Instalar dependencias
    - npm i
* Build
 - ng build
* Compilación
    - ng s --o

El proyecto tiene un diseño simple para facilitar su uso.
En la página principal se pueden visualizar las opciones mas importantes:
 * Registrar un paciente
 * Registrar una vacuna
 * Listado de pacientes
 * Listado de vacunas

Al momento de registrar a un paciente se requieren los datos mas importantes: 
    - Carnet de Identidad
    - Nombre Completo
    - Edad
    - Vacuna que sera suministrada
Luego de registrar a un paciente satisfactoriamente podremos visualizarlo en el listado de pacientes.
En la lista de pacientes se puede realizar una busqueda de acuerdo al Carnet de Identidad.
Al elegir un paciente pueden visualizarse sus datos, como tambien registrar si la vacuna y dosis correspondientes ya fueron suministradas.
Al momento de registrar la primera dosis que recibio un paciente el sistema indicara de acuerdo al intervalo especifico de la vacuna, en que fecha tiene que ser suministrada la segunda dosis.

Para registrar una vacuna se necesitan los siguientes datos:
<<<<<<< HEAD
    - Código de la vacuna
    - Nombre de la vacuna
    - Cantidad de dosis
    - Tiempo de espera para suministrar la segunda dosis en días

En la lista de vacunas se puede acceder a cada una de las vacunas registradas y visualizar sus datos.

## Despliegue 📦

_Agrega notas adicionales sobre como hacer deploy_
=======
- Código de la vacuna
- Nombre de la vacuna
- Cantidad de dosis
- Tiempo de espera para suministrar la segunda dosis en días



En la lista de vacunas se puede acceder a cada una de las vacunas registradas y visualizar sus datos.
>>>>>>> 9de331dd55be82abbefcfb613f07fe9064cdf194

## Construido con 🛠️

* Angular - El framework web utilizado
* NPM - Manejador de dependencias
* Bootstrap - Framework CSS

## Autor ✒️

* **Steffany C. Aparicio** 
